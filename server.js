//----------------Import models-----------------------------------------

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var fs = require("fs");

//----------------Add express middleware layers------------------------

app.use(bodyParser.json());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));

//-----------------Create connection-------------------------------------

var db = mongoose.connect("mongodb://localhost:27017/mean");
if(db){
  console.log("connected to Database");
}


app.get('/index.html', function (req, res) {
   res.sendFile( __dirname + "/" + "index.html" );
});

//---------------------Schemas---------------------------------

var userSchema = mongoose.Schema({
  fname : String,
  lname : String,
  username : String,
  password : String,
});



//-----------------Add user-----------------------------------------
app.post('/api/signup', function (req, res) {
  
   var signup = mongoose.model('user',userSchema);
   console.log(req.body);
   var signupData = new signup(req.body);
    signupData.save(function (err) {
      if(err) throw err;
      res.send("success");
      console.log('user inserted');
    });
});






//-----------------------Default Page----------------------------

app.get('*', function(req, res){
  res.sendFile(__dirname + '/public/index.html', function(err){
    if(err) 
      console.log(err);
  });
});




//----------------Create localhost at port 27017----------------------
var server = app.listen(process.env.PORT || 8081, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log("App listening at %s",port);
});